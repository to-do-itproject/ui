import { expect } from 'chai'
import { mount } from '@vue/test-utils'
import TextInput from '@/components/TextInput.vue'

describe('TextInput.vue', () => {
  it('text input props value', () => {
    const value = 'Text input'
    const wrapper = mount(TextInput, {
      propsData: { valid: true, value: value }
    })
    const elValue = wrapper.find('input').element.value
    expect(wrapper.find('.input').classes()).to.include('valid')
    expect(elValue).to.equal(value)
  })
  it('text input set value', () => {
    const value = 'Text input'
    const wrapper = mount({
      components: {
        TextInput
      },
      data () {
        return {
          input: ''
        }
      },
      render (createElement, context) {
        return (
          <div>
            <text-input v-model={this.input} />
          </div>
        )
      }
    })
    wrapper.find('input').setValue(value)
    expect(wrapper.vm.input).to.equal(value)
  })
})
