import { map, compose, is } from 'ramda'
import { mapKeys } from '@/helpers/camel-case'

export const toSnake = str => {
  return str
    .replace(/\./g, '__')
    .replace(/([A-Z])/g, $1 => '_' + $1.toLowerCase())
}

const snakeCase = (data) => {
  if (is(Array, data)) {
    return map(snakeCase, data)
  }

  if (is(Object, data)) {
    return compose(
      map(snakeCase),
      mapKeys(toSnake)
    )(data)
  }

  return data
}

export default snakeCase
