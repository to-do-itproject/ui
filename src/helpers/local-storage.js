import { is } from 'ramda'

export const getStorage = (key) => {
  const value = localStorage.getItem(key)
  if (value || is(Boolean, value)) {
    try {
      return JSON.parse(value)
    } catch (e) {}
  }
}

export const setStorage = (key, value) => {
  if ((value && key) || is(Boolean, value)) localStorage.setItem(key, JSON.stringify(value))
}

export const removeStorage = key => {
  localStorage.removeItem(key)
}
