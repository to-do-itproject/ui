import {
  is,
  compose,
  curry,
  keys,
  map,
  values,
  zipObj
} from 'ramda'

export const mapKeys = curry((fn, obj) => zipObj(map(fn, keys(obj)), values(obj)))

const camelize = (str) => {
  return str
    .replace(/_/g, ' ')
    .replace(/-/g, ' ')
    .replace(/(?:^\w|[A-Z]|_|\b\w)/g, (letter, index) => {
      return index === 0 ? letter.toLowerCase() : letter.toUpperCase()
    }).replace(/\s+/g, '')
}

const camelCase = (data) => {
  if (is(Array, data)) {
    return map(camelCase, data)
  }

  if (is(Object, data)) {
    return compose(
      map(camelCase),
      mapKeys(camelize)
    )(data)
  }

  return data
}

export default camelCase
