import {
  fromPairs,
  filter,
  toPairs,
  compose,
  is,
  isEmpty,
  pathOr
} from 'ramda'

export const omitEmpty = (obj) => fromPairs(
  filter(pair => {
    if ((!isEmpty(pair[1])) || is(Boolean, pair[1])) return pair
  }, toPairs(obj))
)

export const getCurrentPage = compose(
  Number,
  pathOr(1, ['$route', 'query', 'page'])
)

export const getPageSize = compose(
  Number,
  pathOr(10, ['$route', 'query', 'page_size'])
)
