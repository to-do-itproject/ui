// LOGIN
export const LOGIN_ACTION = 'loginAction'
export const LOGOUT_ACTION = 'logoutAction'

// TO-DO
export const TODO = 'todo'

export const GET_TODO_LIST_ACTION = 'getTodoListAction'
export const TODO_CREATE_ACTION = 'todoCreateAction'
export const TODO_DELETE_ACTION = 'todoDeleteAction'
export const TODO_DETAIL_ACTION = 'todoDetailAction'
export const TODO_UPDATE_ACTION = 'todoUpdateAction'
