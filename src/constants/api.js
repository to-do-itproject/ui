const debug = process.env.NODE_ENV !== 'production'
const BASE_API_URL = process.env.VUE_APP_API_BASE_URL || 'http://127.0.0.1:8000'
export const API_HOST = debug ? BASE_API_URL : 'https://to-do-itproject-api.herokuapp.com'
export const API_VERSION = 'v1'
export const API_URL = `${API_HOST}/api/${API_VERSION}`

// Auth API
const AUTH = 'auth'
export const LOGIN = `/${AUTH}/authenticate/`

// TO-DO API
const TODO = 'todo'

export const GET_TODO_LIST = `/${TODO}/${TODO}/`
export const TODO_CREATE = `/${TODO}/${TODO}/`
export const TODO_DETAIL = `/${TODO}/${TODO}/%d/`
