export const MAIN_ROOT_PATH = '/'

const ID = ':id(\\d+)'

// LOGIN-IN
export const N_LOGIN = 'login'
export const LOGIN_PATH = '/login/'

// CRUD
export const LIST = ''
export const UPDATE = `update/${ID}/`
export const CREATE = 'create/'
export const DETAIL = `${ID}/`

// TODO
export const TODO_PATH = '/'
export const TODO_LIST = 'todo-list'
export const TODO_UPDATE = 'todo-update'
export const TODO_CREATE = 'todo-create'
