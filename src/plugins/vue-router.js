/* ============
 * Vue Router
 * ============
 *
 * The official Router for Vue.js. It deeply integrates with Vue.js core
 * to make building Single Page Applications with Vue.js a breeze.
 *
 * http://router.vuejs.org/en/index.html
 */

import Vue from 'vue'
import { equals } from 'ramda'
import VueRouter from 'vue-router'
import store from '@/store'
import routes from '@/routes'
import { omitEmpty } from '@/helpers/objects'
import { N_LOGIN } from '@/constants/routes'

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  if (location && location.query) {
    location.query = omitEmpty(location.query)
  }
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(VueRouter)

export const router = new VueRouter({
  mode: 'history',
  scrollBehavior: function (to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash
      }
    }
    if (from && equals(to.name, from.name) && equals(to.params, from.params)) {
      return
    }
    return savedPosition || { x: 0, y: 0 }
  },
  routes
})

router.beforeEach((to, from, next) => {
  const params = to.params
  if (to.matched.some(m => m.meta.auth)) {
    const token = store.getters.getToken
    const redirect = to.path
    if (token) {
      return next({
        params
      })
    }
    return next({
      name: N_LOGIN,
      query: {
        redirect
      },
      params
    })
  }
  return next({
    params
  })
})

Vue.router = router

export default router
