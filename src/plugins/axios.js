import axios from 'axios'
import { path, equals, is, compose, prop } from 'ramda'
import { API_URL } from '@/constants/api'
import VueRouter from '@/plugins/vue-router'
import store from '@/store'
import camelCase from '@/helpers/camel-case'
import { LOGOUT_ACTION } from '@/constants/actionTypes'
import * as ROUTES from '@/constants/routes'

const UNAUTHORIZED = 401
const CONTENT_TYPE_JSON = 'application/json'

const responseToCamelCase = (data, response) => {
  const responseContentType = path(['content-type'], response)

  if (equals(CONTENT_TYPE_JSON, responseContentType)) {
    return camelCase(JSON.parse(data))
  }

  if (is(Object, data) || is(Array, data)) {
    return camelCase(data)
  }

  return data
}

const errorInterceptors = async error => {
  const status = path(['response', 'status'], error)

  if (equals(UNAUTHORIZED, status)) {
    await store.dispatch(LOGOUT_ACTION)
    return VueRouter.push({
      name: ROUTES.N_LOGIN
    })
  }

  return Promise.reject(error)
}

axios.defaults.transformResponse = [responseToCamelCase]

axios.defaults.baseURL = API_URL

axios.interceptors.request.use(request => {
  const token = store.getters.getToken
  if (token) {
    request.headers.common.Authorization = `Token ${token}`
  }
  return request
})

axios.interceptors.response.use(
  response => response,
  errorInterceptors
)

export const getPayloadFromSuccess = prop('data')
export const getPayloadFromError = compose(
  data => Promise.reject(data),
  path(['response', 'data'])
)

export default axios
