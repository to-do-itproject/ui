/* ============
 * Lists routes
 * ============
 *
 */
import RouterRoot from '@/components/RouterRoot'

import * as PAGE from '@/views/todo'
import * as ROUTE from '@/constants/routes'

export default [
  {
    path: ROUTE.TODO_PATH,
    component: RouterRoot,
    children: [
      {
        path: ROUTE.LIST,
        name: ROUTE.TODO_LIST,
        component: PAGE.TodoList
      },
      {
        path: ROUTE.CREATE,
        name: ROUTE.TODO_CREATE,
        component: PAGE.TodoCreate
      },
      {
        path: ROUTE.UPDATE,
        name: ROUTE.TODO_UPDATE,
        component: PAGE.TodoUpdate
      }
    ]
  }
]
