/* ============
 * Routes File
 * ============
 *
 */
import AdminLayout from '@/components/layouts/AdminLayout'
import Login from '@/views/login'
import * as ROUTE from '@/constants/routes'

import Todo from './todo'

import store from '@/store'

export default [
  {
    path: ROUTE.MAIN_ROOT_PATH,
    component: AdminLayout,
    meta: {
      auth: true
    },
    children: [
      ...Todo
    ]
  },
  {
    name: ROUTE.N_LOGIN,
    path: ROUTE.LOGIN_PATH,
    component: Login,
    beforeEnter: async (to, from, next) => {
      const token = store.getters.getToken
      if (token) {
        return next('/')
      }
      return next()
    }
  }
]
