import Vue from 'vue'

/* ============
 * Plugins
 * ============
 */

import '@/plugins/vuex'
import '@/plugins/vuex-router-sync'
import router from '@/plugins/vue-router'
import '@/plugins/vee-validate'

/* ============
 * Styles
 * ============
 */
import '@/styles/main.scss'

/* ============
 * Main App
 * ============
 *
 * Import the main application.
 */
import App from '@/App'
import store from '@/store'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
