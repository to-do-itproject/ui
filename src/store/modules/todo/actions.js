/* ============
 * Actions for the todo state
 * ============
 *
 */

import { path } from 'ramda'
import { sprintf } from 'sprintf-js'
import axios from '@/plugins/axios'
import router from '@/plugins/vue-router'
import snakeCase from '@/helpers/snake-case'
import * as ACTION from '@/constants/actionTypes'
import * as API from '@/constants/api'
import { SET_TODO_DETAIL, SET_TODO_LIST } from './mutation-types'

export default {
  [ACTION.GET_TODO_LIST_ACTION] ({ commit }) {
    const params = path(['history', 'current', 'query'], router)
    commit(SET_TODO_LIST, [])
    return axios.get(API.GET_TODO_LIST, { params })
      .then(response => {
        commit(SET_TODO_LIST, response.data)
      })
  },
  [ACTION.TODO_CREATE_ACTION] ({ commit }, form) {
    return axios.post(API.TODO_CREATE, snakeCase(form))
  },
  [ACTION.TODO_DETAIL_ACTION] ({ commit }, id) {
    const url = sprintf(API.TODO_DETAIL, id)
    commit(SET_TODO_DETAIL, {})
    return axios.get(url).then(response => {
      commit(SET_TODO_DETAIL, response.data)
      return response
    })
  },
  [ACTION.TODO_UPDATE_ACTION] ({ commit }, id) {
    const url = sprintf(API.TODO_DETAIL, id)
    return (form) => axios.patch(url, snakeCase(form)).then(() => {
      commit(SET_TODO_DETAIL, {})
    })
  },
  [ACTION.TODO_DELETE_ACTION] ({ commit, dispatch }, id) {
    const url = sprintf(API.TODO_DETAIL, id)
    commit(SET_TODO_LIST, [])
    return axios.delete(url).then(() => {
    }).catch(() => {
    }).finally(() => {
      dispatch(ACTION.GET_TODO_LIST_ACTION)
    })
  }
}
