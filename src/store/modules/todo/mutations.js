/* ============
 * Mutations for the todo state
 * ============
 *
 * The mutations that are available on the
 * todo state.
 */

import {
  SET_TODO_LIST,
  SET_TODO_DETAIL
} from './mutation-types'

/* eslint-disable no-param-reassign */
export default {
  [SET_TODO_LIST] (state, list) {
    state.list = list
  },
  [SET_TODO_DETAIL] (state, detail) {
    state.detail = detail
  }
}
