/* ============
 * Mutation types for the todo state
 * ============
 *
 */

export const SET_TODO_LIST = 'SET_TODO_LIST'
export const SET_TODO_DETAIL = 'SET_TODO_DETAIL'

export default {
  SET_TODO_LIST,
  SET_TODO_DETAIL
}
