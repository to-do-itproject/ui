/* ============
 * State of the todo module
 * ============
 *
 * The initial state of the todo module
 */

export default {
  list: {},
  detail: null
}
