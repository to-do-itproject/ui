/* ============
 * Getters for the general state
 * ============
 *
 */

export const getToken = state => state.token

export default {
  getToken
}
