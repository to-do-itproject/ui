/* ============
 * Mutations for the general state
 * ============
 *
 * The mutations that are available on the
 * general state.
 */

import {
  LOG_IN,
  LOG_OUT
} from './mutation-types'
import { removeStorage, setStorage } from '@/helpers/local-storage'
import { removeSession, setSession } from '@/helpers/local-session'

export default {
  [LOG_IN] (state, { token, remember }) {
    state.token = token
    if (remember) {
      return setStorage('token', token)
    }
    return setSession('token', token)
  },
  [LOG_OUT] (state) {
    state.token = null
    removeStorage('token')
    removeSession('token')
  }
}
