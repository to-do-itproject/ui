/* ============
 * Mutation types for the general state
 * ============
 *
 */

export const LOG_IN = 'LOG_IN'
export const LOG_OUT = 'LOG_OUT'

export default {
  LOG_IN,
  LOG_OUT
}
