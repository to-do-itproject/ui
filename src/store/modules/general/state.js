/* ============
 * State of the general state
 * ============
 *
 * The initial state of the general state
 */

import { getStorage } from '@/helpers/local-storage'
import { getSession } from '@/helpers/local-session'

export default {
  token: getSession('token') || getStorage('token')
}
