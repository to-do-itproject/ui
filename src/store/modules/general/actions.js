/* ============
 * Actions for the general state
 * ============
 *
 */

import { path } from 'ramda'
import { router } from '@/plugins/vue-router'
import axios from '@/plugins/axios'
import * as ACTION from '@/constants/actionTypes'
import * as API from '@/constants/api'
import { LOG_IN, LOG_OUT } from './mutation-types'
import { N_LOGIN } from '@/constants/routes'

export default {
  [ACTION.LOGIN_ACTION] ({ commit, dispatch, state }, { remember, ...params }) {
    return axios.post(API.LOGIN, params)
      .then(response => {
        const token = path(['data', 'token'], response)
        commit(LOG_IN, {
          token,
          remember
        })
        router.push('/')
      })
  },
  [ACTION.LOGOUT_ACTION] ({ commit }) {
    commit(LOG_OUT)
    router.push({
      name: N_LOGIN
    })
  }
}
