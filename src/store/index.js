/* ============
 * Vuex Store
 * ============
 *
 * The store of the application.
 *
 * http://vuex.vuejs.org/en/index.html
 */

import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

// Namespaces
import * as NAMESPACE from '@/constants/actionTypes'

// Modules
import general from './modules/general'
import todo from './modules/todo'

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  ...general,

  modules: {
    [NAMESPACE.TODO]: todo
  },

  /**
   * If strict mode should be enabled.
   */
  strict: debug,
  devtools: debug,

  /**
   * Plugins used in the store.
   */
  plugins: debug ? [createLogger()] : []
})
