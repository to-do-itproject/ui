/* ============
 * Mixins for working with text
 * ============
 *
 */
import { is } from 'ramda'

export default {
  filters: {
    slice: function (text, length) {
      if (!is(String, text)) return
      if (text.length >= length) {
        return text.slice(0, length) + '...'
      }
      return text
    }
  }
}
