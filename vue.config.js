const path = require('path')

module.exports = {
  configureWebpack: {
    resolve: {
      extensions: ['.js', '.vue', '.json'],
      alias: {
        '~': path.resolve(__dirname, 'src/'),
        '@': path.resolve(__dirname, 'src/')
      }
    }
  },
  transpileDependencies: [
    'vuetify'
  ],
  css: {
    loaderOptions: {
      sass: {
        prependData: "@import '@/styles/variables.scss'"
      },
      scss: {
        prependData: "@import '@/styles/variables.scss';"
      }
    }
  }
}
